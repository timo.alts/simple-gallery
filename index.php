<?php
/**
 * Created by PhpStorm.
 * User: swamp
 * Date: 29.04.2016
 * Time: 11:06
 */
error_reporting(1);
function endsWith($haystack, $needle)
{
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}
$tokens = explode('/', $_SERVER['PHP_SELF']);      // split string on :
array_pop($tokens);                   // get rid of last element
$baseurl = implode('/', $tokens);   // wrap back

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Simple Gallery</title>

    <!-- Bootstrap -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<section id="uploadform">
    <div class="container">
        <div class="row">
            <div class="container-fluid text-center">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form class="form-inline" action="upload.php" method="post" enctype="multipart/form-data">
                        <input type="text" class="form-control" id="inputfoldername" name="inputfoldername"
                               placeholder="Default Folder">
                        <span class="btn btn-default btn-file"><input type="file" name="fileToUpload" id="fileToUpload"></span>
                        <input type="submit" value="Upload Image" name="submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="folderlist">
    <div class="container">
        <div class="row">
            <div class="container-fluid text-center custompadding">
                <div class="btn-group" data-toggle="buttons">
                    <label class=" btn btn-default">
                        <input type="radio" name="selectfolder" class="folderselectradio" value="">
                        All
                    </label>
                    <?php
                    $dirs = array_filter(glob('uploads/*'), 'is_dir');
                    foreach ($dirs as $dir) {
                        $contents = explode('/', $dir);
                        ?>
                        <label class=" btn btn-default">
                            <input type="radio" name="selectfolder" class="folderselectradio"
                                   value="<?php echo end($contents); ?>">
                            <?php echo end($contents); ?>
                        </label>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="gallery view">
    <div class="container">
        <div class="row">
            <div class="container-fluid custompadding">
                <?php
                if (isset($_GET['folder'])) {
                    $viewdir = "uploads/" . $_GET['folder'].'/';
                } else {
                    $viewdir = "uploads/";
                }

                $it = new \RecursiveIteratorIterator(
                    new \RecursiveDirectoryIterator($viewdir),
                    \RecursiveIteratorIterator::CHILD_FIRST
                );
                $it->rewind();
                while ($it->valid()) {
                    if ($it->current()->getBasename() != "." && $it->current()->getBasename() != ".." && !is_dir($it->current())) {
                        ?>
                        <div class="col-lg-4 col-md-4 col-xs-4 " data-toggle="modal"
                             data-target="#imgModal<?php echo $count; ?>">
                            <img src="<?php echo $it->current()->getPathname(); ?>" class="img-responsive">
                        </div>
                        <!-- Modal -->
                        <div class="modal fade"
                            <?php echo 'id="imgModal' . $count . '"'; ?> tabindex="-1"
                             role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Image
                                            <?php echo $it->current()->getBasename() . PHP_EOL; ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?php echo $it->current()->getPathname(); ?>"
                                             class="img-responsive">
                                        <?php echo $it->current()->getSize() . ' bytes <br/>'; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    $count++;
                    $it->next();
                }


                ?>
            </div>
        </div>
    </div>
</section>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('input:radio[name="selectfolder"]').change(function () {
            window.location.href = 'index.php?folder=' + $(this).val();
        });
    });
</script>
</body>
</html>
